# debian-ezgo-temp

Repository for debian ezgo packages.

If you have blends-dev installed in your Debian, you can use

`# debuild -us -uc`

in the first level folder to build the whole ezgo deb packages, except `ezgo-menu` and `ubiquity-slideshow-ezgo`, which have their own debian file and should be built in ezgo-menu/ and ubiquity-slideshow-ezgo/ separately.