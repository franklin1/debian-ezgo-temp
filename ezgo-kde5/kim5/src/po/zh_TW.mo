��    ;      �  O   �           	      (     I     h     |     �     �     �     �  
   �     �            
   -  
   8     C     b  #   t     �     �  /   �  &   �  	   &     0     4     B     [     r     �     �     �     �     �          #  %   9     _     u     �     �     �  /   �     	     	     	     6	     S	     l	     z	     �	     �	     �	  "   �	     �	  o   
      �
     �
     �
  @  �
      �  "         7     X     i     z     �     �     �     �     �     �     �               ,     L     \     {     �  -   �  !   �     �     
               :     V     o     �     �     �     �     �  !   �  '     '   7     _     {     �     �  B   �          "     2     N  !   m     �     �     �     �     �  !   	     +  h   >     �     �     �        ;   &   +   (      :   .                  1           /   5                7   %       4       6                    '              3   0                 -                !      $   "      #      )      
                    ,   *              	   2   9          8                    10x15 cm: 1x2 images, portrait 11.5x15 cm: 1x2 images, portrait 13x18 cm: 1x2 images, portrait 1x1 landscape album 1x1 portrait album 1x2 landscape album 20x25 cm: 1 image, landscape 2x1 portrait album 2x2 landscape album 300x225 px 3x2 portrait album 3x4 landscape album 4x4 landscape album 600x450 px 800x600 px 9x13 cm: 2x2 images, landscape Background color: Choose duration of animation (sec): Choose the new name of images: Choose your annotation: Click 'Ok' and then choose the window target !  Do you want to replace existing files? Full size Kim Kim - Author: Kim - Compressing file:  Kim - Converting file: Kim - Creating album ... Kim - Creating miniatures ... Kim - Deleting miniatures ... Kim - Initialising ... Kim - Initialising compression Kim - Number of columns: Kim - Record Kim - Renaming file:  Kim - Resizing and compressing file:  Kim - Resizing file:  Kim - Rotating file:  Kim - Select album type: Kim - Select border color: Kim - Send by mail: Kim - Send by mail: you can write your message! Kim - Size: Kim - Title: Kim - Treatment of file:  Kim - Unrecognized option !  Kim - resizing of file:  My annotation My title ... Number of rows and columns: Overlaping ratio in percent: Size of small images: The action was cancelled by user ! The animation is created !  This feature is supposed to create a Flash slideshow, but Flash is dead now!  Are you sure you want to do that? Thumbnails: 4x5 images, portrait black white Project-Id-Version: kim 5
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-08-20 22:13+0800
Last-Translator: Franklin Weng <franklin@goodhorse.idv.tw>
Language-Team: Chinese (traditional) <zh_l10n@lists.linux.org.tw>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 10x15 cm: 1x2 張影像，直向 11.5x15 cm: 1x2 張影像，直向 13x18 cm: 1x2 張影像，直向 1x1 橫向相簿 1x1 直向相簿 1x2 橫向相簿 20x25 cm: 1 張影像，橫向 2x1 直向相簿 2x2 橫向相簿 300x225 像素 3x2 直向相簿 3x4 橫向相簿 4x4 橫向相簿 600x450 像素 800x600 像素 9x13 cm: 2x2 張影像，橫向 背景顏色： 選擇動畫時間（秒）： 選擇新影像的名稱： 選擇您的註記： 點擊「確認」然後選擇視窗目標。 您要取代現有的檔案嗎？ 完整大小 Kim Kim - 作者： Kim - 壓縮此檔案中： Kim - 轉換此檔案中： Kim - 建立相簿中... Kim - 建立縮圖中... Kim - 刪除縮圖中... Kim - 初始化中... Kim - 壓縮初始化中 Kim - 欄數： Kim - 錄影 Kim - 重新命名此檔案中： Kim - 正在調整與壓縮此檔案： Kim - 重新調整此檔案大小中： Kim - 旋轉此檔案中： Kim - 請選擇相簿型態： Kim - 選擇邊框顏色： Kim - 用郵件寄出： Kim - 用郵件寄出：您可以開始寫您的信件內容了。 Kim - 大小： Kim - 標題： Kim - 檔案處理方式： Kim - 無法識別的選項。 Kim - 調整此檔案大小中： 請將註記寫在這裡 請在此輸入標題... 列數與欄數： 覆蓋比例（百分比）： 小影像的大小： 此動作已被使用者取消。 動畫已建立。 這個功能是用 Flash 建立投影秀，但現在 Flash 已經死翹翹了！您確定要繼續嗎？ 縮圖：4x5 張影像，直向 黑色 白色 